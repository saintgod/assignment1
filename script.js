//UI that the API uses
let computerSelect = document.getElementById("computers")
let specsPTag = document.getElementById("Specs")
let computerInfoList = document.getElementById("computerSelectionList")
let computerImage = document.getElementById("image")
let infoSectionName = document.getElementById("infoSectionName")
let infoSectionDescription = document.getElementById("infoSectionDescription")
let price = document.getElementById("price")

let loanButton = document.getElementById("loanButton")
let loanElement = document.getElementById("loanText")
let loanText = document.getElementById("loan")
let repayLoanBtn = document.getElementById("payBLoanButton")
let workButton = document.getElementById("workButton")
let bankButton = document.getElementById("bankButton")
let buyButton = document.getElementById("buyButton")

let computers = []
let loanAmount = parseInt(loanText.innerText)
let workBalance = parseInt(document.getElementById("workMoney").innerText)
let balance = parseInt(document.getElementById("balance").innerText)
window.addEventListener('DOMContentLoaded', (event) => {
    console.log('DOM fully loaded and parsed')
    balance = 500
    repayLoanBtn.classList.add("hide")
    fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputersToUI(computers))
   
});
const addComputersToUI = (computers) => {
    computers.forEach(x => addComputerToUI(x))
    handleComputerChange(computers[0])
}
const addComputerToUI = (computer) => {
    const computerElement = document.createElement("option")
    computerElement.value = computer.id
    computerElement.appendChild(document.createTextNode(computer.title))
    computerSelect.appendChild(computerElement)
    
}
const handleComputerChange = computer => {
    const selectedComputer = computer
    console.log(selectedComputer)
    //Reset all text before altering
    computerInfoList.innerHTML = ""
    computerImage.innerHTML = ""
    infoSectionName.innerHTML = ""
    infoSectionDescription.innerHTML = ""
    price.innerHTML = ""
    //Altering 
    price.innerText = selectedComputer.price + " kr"
    infoSectionName.innerText = selectedComputer.title
    infoSectionDescription.innerText = selectedComputer.description
    let computerImgElement = document.createElement("img")
    computerImgElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedComputer.image
    computerImgElement.height = 200
    computerImgElement.width = 200
    computerImage.appendChild(computerImgElement)
    for(let i in selectedComputer.specs){
        let computerInfoListElement = document.createElement("li")
        computerInfoListElement.appendChild(document.createTextNode(selectedComputer.specs[i]))
        computerInfoList.appendChild(computerInfoListElement)
    }
}
computerSelect.addEventListener("change", e => {
    let computer = computers[e.target.selectedIndex]
    handleComputerChange(computer)
})

function checkIfLoanIsValid(loanAmount){
    console.log(loanAmount)
    if((loanAmount/2) > balance){
        return false
    }else{
        return true
    }
}    
function updateWorkBalance(){
    document.getElementById("workMoney").innerText = "" + workBalance
}
//Uppdateras när ett lån återbetalas
function updateLoan(){
    loanText.innerText = "" + loanAmount
}
function updateLoanUI(){
    if(parseInt(loanText.innerText) == 0){
        loanElement.classList.add("hide")
        repayLoanBtn.classList.add("hide")
    }else{
        loanElement.classList.remove("hide")
        repayLoanBtn.classList.remove("hide")
    }
      
    
}
function updateBalance(){
    document.getElementById("balance").innerText = balance
}
workButton.addEventListener("click", function(){
    workBalance += 100
    updateWorkBalance()
})
bankButton.addEventListener("click", function(){
    let difference = (workBalance/10) 
    let remaining = 0 
    if(difference >= loanAmount){
        remaining = difference - loanAmount
        loanAmount = 0
    }else{
        loanAmount -= difference
    }

    
    balance += workBalance + remaining - difference 
    workBalance = 0
    updateBalance()
    updateWorkBalance()
    updateLoan()
    updateLoanUI()

})
repayLoanBtn.addEventListener("click", function(){
    let remaining = 0
    if(workBalance >= loanAmount){
        remaining = workBalance - loanAmount
        balance += remaining
        loanAmount = 0
    }        
    else{
        loanAmount -= workBalance        
    }
        workBalance = 0
        updateBalance()
        updateLoan()
        updateWorkBalance()
        updateLoanUI()
})
loanButton.addEventListener("click", function(){
    let loanAmt = window.prompt("Please Enter Loan Amount: ")
    loanAmount = parseInt(loanAmt)
    console.log(checkIfLoanIsValid(loanAmount))
    if(checkIfLoanIsValid(loanAmount)){
        balance += loanAmount
        updateBalance()
        updateLoan()       

    }else{
        console.log("Here!")
        alert("You cant loan like that :(")
    }
    updateLoanUI()


})
buyButton.addEventListener("click", function(){
    let computerCost = parseInt(price.innerText)
    console.log(computerCost-balance)
    if(balance >= computerCost){
        balance -= computerCost
        alert("You are now the proud owner of a: " + infoSectionName.innerText)
    }else{
        alert("You do not have sufficient balance to buy this computer, you need " + (computerCost-balance) + " kr to successfully buy it")
    }
    updateBalance()
    
})